<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	public function index()
	{
			$data = [];
			$this->load->model('Main_model','main_model');
			$data['codes'] = $this->main_model->get_all_codes();
			$data['login'] = $this->ion_auth->logged_in();
			$this->load->view('templates/header',$data);
			$this->load->view('admin');
			$this->load->view('templates/footer');
	}
}
