<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->load->model('Main_model','main_model');
	}


	public function index()
	{	
		$loged = [];
		$loged['login'] = $this->ion_auth->logged_in();
		$this->load->view('templates/header',$loged);
		$this->load->view('verify');
		$this->load->view('templates/footer');
	}

	public function verify($code){
		$result = $this->main_model->verify_code($code);

		if ($result) {
			echo "<div class='c-green'><h2>Valid!</h2><span>Your Product is Valid</span></div>";
		}else{
			echo "<div class='c-red'><h2>Oopps!</h2><span>Your Product is not Valid</span></div>";
		}
	}

}
