<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model
{
	public function __construct()
	{
		$this->config->load('ion_auth', TRUE);
		$this->load->helper('cookie', 'date');
		$this->lang->load('ion_auth');
	}

	public function verify_code($code){
		if (empty($code)) {
			return false;
		}
		$condition = array("product_name" => $code, 'valid' => '1');
		$query =  $this->db->where($condition)
		->limit(1)
		->get('products');

		if ($query->num_rows() == 0)
		{
			return FALSE;
		}else{
			$row = $query->row_array();
			$id = $row['id'];
			return $this->do_expire($id);
		}
	}

	private function do_expire($id,$val=null){
		if($val==null){
			return $this->db->update('products',['valid'=>'0'],['id'=>$id]);
		}else if($val!=null || $val != ""){
			($val==false) ? 0 : 1;
			return $this->db->update('products',['valid'=>$val],['id'=>$id]);
		}
	}

	public function get_all_codes(){
		$query = $this->db->get('products');
		return $query->result();
	}
}