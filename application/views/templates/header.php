<!DOCTYPE html>
<html>
<head>
	<title>Product Verification</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/sweetalert/sweetalert.css">
<!--===============================================================================================-->
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
<!--===============================================================================================-->
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-warning">
  	<a class="navbar-brand" href="<?php echo base_url('index.php'); ?>">Product Verification System</a>
  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
  	</button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav ml-auto">
      	<li class="nav-item">
       		<a class="nav-link" href="<?php echo base_url('index.php'); ?>"><i class="fa fa-home fa-3x"></i><p>Home</p></a>
      	</li>
      	<li class="nav-item">
        	<a class="nav-link" href="<?php echo base_url('index.php/contact'); ?>"><i class="fa fa-phone fa-3x"></i><p>Contact</p></a>
      	</li>
      	<?php if(isset($login) && $login != 1){ ?>
      	<li class="nav-item">
        	<a class="nav-link" href="<?php echo base_url('index.php/auth/login'); ?>"><i class="fa fa-sign-in fa-3x"></i><p>Sign In</p></a>
      	</li>
      <?php }else if(!isset($login)){ ?>
      	<li class="nav-item">
        	<a class="nav-link" href="<?php echo base_url('index.php/auth/login'); ?>"><i class="fa fa-sign-in fa-3x"></i><p>Sign In</p></a>
      	</li>
      <?php } else { ?>
      	<li class="nav-item">
        	<a class="nav-link" href="<?php echo base_url('index.php/admin'); ?>"><i class="fa fa-dashboard fa-3x"></i><p>Dashboard</p></a>
      	</li>
      	<li class="nav-item">
        	<a class="nav-link" href="<?php echo base_url('index.php/auth/logout'); ?>"><i class="fa fa-sign-out fa-3x"></i><p>Sign Out</p></a>
      	</li>
      <?php } ?>
    </ul>
  </div>
	</nav>

	
