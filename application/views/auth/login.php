<div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100 p-t-50 p-b-90">
        <div class="login100-form validate-form flex-sb flex-w">
          <?php echo form_open("auth/login");?>
          <span class="login100-form-title p-b-51">
            <?php echo lang('login_heading');?>
          </span>
          <?php if($message != ''){ ?>
          <div id="infoMessage" class="alert alert-danger text-center"><?php echo $message;?></div>
          <?php } ?>
          <div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
            <?php echo form_input($identity,'','placeholder=Email');?>
            <span class="focus-input100"></span>
          </div>
          
          
          <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
            <?php echo form_input($password,'','placeholder=Password');?>
            <span class="focus-input100"></span>
          </div>
          
          <div class="flex-sb-m w-full p-t-3 p-b-24">
            <div class="contact100-form-checkbox">
              <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember">
              <label class="label-checkbox100" for="ckb1">
                Remember me
              </label>
            </div>

            <div>
              <a href="forgot_password" class="txt1">
                Forgot?
              </a>
            </div>
          </div>

          <div class="container-login100-form-btn m-t-17">
            <input class="login100-form-btn" type="submit" name="submit" value="LOGIN" />
          </div>

        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>