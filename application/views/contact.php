<div id="bg-info" class="p-banner text-center bg-light">
		<span class="p-banner-title fz-28">Contact Us</span>
	</div>
	<div class="container over-top" id="contact-area">
			<div class="row">
				<div class="col-md-8" id="contact-form">
					<div class="max-w-75">
					<h2 class="pt-4">Send us a message</h2>
					<p id="site_description">You can contact us with anything related to our Products. We'll get in touch with you as soon as possible.
					</p>	
					<form>
						<div class="form-group input-group">
							<!-- <label for="name">Name:</label> -->
							<input type="text" id="name" name="name" class="form-control" placeholder="Name">
						</div>
						<div class="form-group input-group">
							<!-- <label for="email">Email:</label> -->
							<input type="email" id="email" name="email" class="form-control" placeholder="Email">
						</div>
						<div class="form-group input-group">
							<!-- <label for="phone">Phone:</label> -->
							<input type="text" id="phone" name="phone" class="form-control" placeholder="Phone">
						</div>
						<div class="form-group input-group">
							<!-- <label for="message">Message:</label> -->
							<textarea class="form-control" id="message" placeholder="Enter Your Message here">
								
							</textarea>
						</div>

						<button class="btn bg-orange c-white">SEND</button>
					</form>
					</div>
				</div><!-- /.col-md-8 -->
				<div class="col-md-4 c-white bg-linear-royal" id="contact-detail">
						<div class="max-w-80">
							<div id="contact-location">
							<h4><i class=" fa fa-map-marker"></i> Find us at the office</h4>
							<p>Address Line 1</p>
							<p>Address Line 2</p>
							<p>City : XYZ</p>
						</div>
						<div id="contact-phone">
							<h4><i class=" fa fa-phone"></i> Give us a ring</h4>
							<p>Owner Name</p>
							<p>+9x xxx xxx xxxx</p>
						</div>
						<div id="contact-legale">
							<h4><i class=" fa fa-briefcase"></i> Legal information</h4>
							<p>Brand Name</p>
							<p><i>Brand Slogan</i></p>
							<p>MON-FRI 8:00-22:00</p>
						</div>
						</div>
					</div><!-- /.col-md-4 -->
				</div>
			</div>