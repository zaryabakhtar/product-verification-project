<div class="container">
	
	<!-- <div class="margin-3em">
		<label>Upload New Codes</label>
		<input type="file" name="new_codes">
	</div> -->
	<table class="table table-responsive table-striped table-bordered mt-4"> 
		<tr>
			<th class="col-1">ID</th>
			<th class="col-10">Code #</th>
			<th class="col-1">Status</th>
		</tr>
		<?php foreach ($codes as $value) { ?>
		<tr>
			<td><?php echo $value->id ?></td>
			<td><?php echo $value->product_name ?></td>
			<td><input id="<?php echo $value->id; ?>" type="checkbox" <?php if($value->valid == 1){echo "checked";} ?> data-toggle="toggle" disabled data-onstyle="success" data-offstyle="danger" data-on="Valid" data-off="Used"></td>
		</tr>
	<?php } ?>
	</table>
</div>