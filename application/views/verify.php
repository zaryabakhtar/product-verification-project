
<div class="max-w-75 margin-3em">
	<div>
		<h1 class="text-center">Verify Your Product</h1>
		<p class="text-center c-black"><i>Only get the Verified Products</i></p>
		<form id="verification-form">
			<div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
         		<?php echo form_input("Code",'','placeholder="Enter Your Code Here" id="code"');?>
            	<span class="focus-input100"></span>
    		</div>
    		<input type="submit" name="submit" class="btn btn-warning c-white pull-right btn-fancy" value="Test Verification">
		</form>
	</div>
</div>

<div class="max-w-75 margin-3em" id="code-response">
	
</div>