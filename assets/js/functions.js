$("#verification-form").on('submit',function(e){
	e.preventDefault();
	if($("#code").val() == ""){
		swal({
			title: 'Oops!',
			text: 'Please Enter Your Code',
			type: 'error'
		});
		$("#code").focus();
	}else if($("#code").val().length < 9){
		swal({
			title: 'Oops!',
			text: 'Please Enter 9 Digit Product Code',
			type: 'error'
		});
		$("#code").focus();
	}else{
		var code = $("#code").val();
		$.ajax({
			method: 'POST',
			url: 'index.php/home/verify/'+code,
			success:function(data){
				if(data){
					$("#code-response").html(data);
				}else{
					$("#code-response").html(data);
				}
			},
			error:function(){

			}
		});
	}
});
